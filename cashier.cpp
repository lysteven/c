// Steven Ly
// Class TR at 9:00 am
// Homework Three
// Write a program that directs a cashier how to give change
#include <iostream>
#include <string>
#include <ctime>

using namespace std;
// Set up memory
double amountDue,amountPaid,change,dollars, quarters,dimes, nickels, pennies;
char answer;
//Staff info
string name, id;

// current date/time based on current system
time_t now = time(0);
// convert now to string form
char* dt = ctime(&now);

//Base class	
class Cashier{
public:
	void setName(string n) {
		name = n;
	}
	void setID(string id_new) {
		id =id_new;
	}

public:
	void  calculate() {
		do
		{
			// Ask user to enter amount due , amount paid
			cout << "Please enter the amount due : ";
			cin >> amountDue;
			cout << "Please enter the amount paid : ";
			cin >> amountPaid;
			// Do calculations

			change = (amountPaid - amountDue) * 100;
			dollars = int(change / 100);
			quarters = ((int(change - (dollars * 100))) / 25);
			dimes = ((int(change - (dollars * 100) - (quarters * 25))) / 10);
			nickels = ((int(change - (dollars * 100) - (quarters * 25) - (dimes * 10))) / 5);
			pennies = int(change - (dollars * 100) - (quarters * 25) - (dimes * 10) - (nickels * 5));

			if (amountDue > amountPaid) {
				cout << "Oops! Please pay a correct amount";
			}
			else {

				// Print out to the screen
				cout << "\n\nThe change due to the customer is ";
				cout << "\n" << dollars << " dollars ";
				cout << "\n" << quarters << " quarters ";
				cout << "\n" << dimes << " dimes ";
				cout << "\n" << nickels << " nickels ";
				cout << "\n" << pennies << " pennies "
					<< endl;
			}
			// Return
			cout << "\nReturn ? (y or n )";
			cin >> answer;
		} while (answer == 'y');

	};


};

// Derived class
class Staff : public Cashier {
public:
	string getInfo() {
		return name +" "+ id;
	}
};

int main()
{
	Staff s;
	// Print the info of staff.
		cout << "This is simple programm for calculating changes\n";
		cout << "Please enter cashier name : ";
		cin >> name;
		cout << "Please enter cashier ID : ";
		cin >> id;
		s.setName(name);
		s.setID(id);
		cout << "Hello " << s.getInfo() <<" "<< dt << "\n";
	

	Cashier c;
	c.calculate();
	return 0;
}


