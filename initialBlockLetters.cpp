// Huy Ly\\
// Class TR at 9:00
// Homework Four
// Write a program that displays each of your initials in block letter form
#include<iostream>
using namespace std;
/************************* FUNCTION PROTOTYPES****************************/
void Explain();
void  FirstInitial();
void MiddleInitial();
void LastInitial();
void YourInitial();
void initial();
/*************************************************************************/

int main ()
{

	char answer;
	char first;
	char middle;
	char last;
	do
	{

		Explain(); // go to my Explain() function and then come back here
		cout << "Please enter your first initial , middle initial and last initial :";
		cin>>first>>middle>>last;


		if ((first=='h'||first=='H') &&( middle=='d'||middle=='D') &&(last=='l'||last=='L'))
		{FirstInitial();
		MiddleInitial();
		LastInitial();}
		else
			cout << "You have made an invalid enter "
			<<endl;



		cout<< "\nDo you want to continue ? ( y or n ): ";
		cin>>answer;
	}while(answer =='y');

	return 0; // End the program
}


void  FirstInitial()
{
	cout<<"\t  ***       ***     \n"
		<< "\t  ***       ***     \n"
		<< "\t  ***       ***     \n"
		<< "\t  ***       ***     \n"
		<< "\t  *** ***** ***     \n"
		<< "\t  ***       ***     \n"
		<< "\t  ***       ***     \n"
		<< "\t  ***       ***     \n"
		<< "\t  ***       ***     \n"
		<< "\t  ***       ***     \n";



	return; 

}
void MiddleInitial()
{

	cout   <<"\n\t  *** ******            \n"
		<< "\t  ***        **          \n"
		<< "\t  ***         **        \n"
		<< "\t  ***          **       \n"
		<< "\t  ***          **       \n"
		<< "\t  ***          **       \n"
		<< "\t  ***          **       \n"
		<< "\t  ***         **        \n"
		<< "\t  ***        **          \n"
		<< "\t  *** ******            \n";


	return;

}
void LastInitial()
{
	cout<<   "\n\t  ***              \n"
		<< "\t  ***              \n"
		<< "\t  ***              \n"
		<< "\t  ***              \n"
		<< "\t  ***              \n"
		<< "\t  ***              \n"
		<< "\t  ***              \n"
		<< "\t  ***              \n"
		<< "\t  *** ********      \n"
		<< "\t  *** ********      \n";
	return;
}



void YourInitial() 
	// this routine will call all of first , middle and last initial of your name
	// It calls FirstInitial() , MiddleInitial() , LastInitial()
{
	FirstInitial();
	MiddleInitial();
	LastInitial();

	return;
}
void Explain()
{
	cout << "\nThis program uses functions to display my initials in block form : \n";

	return;
}



