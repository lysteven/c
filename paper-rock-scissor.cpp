// Huy Ly
// Class TR at 9:00 am
// Homework Three
// Write a program to score the paper-rock-scissors game
#include<iostream>
using namespace std;
int main ()
{
	// Set up memory
	char one;
	char two;
	char answer;
	int total;
	int won1;
	int won2;
	int noWon;
	total=0;
	won1=0;
	won2=0;
	noWon=0;
	do // Loop
	{
		// Print out to the screen
		cout << "\nLet's play the rock , paper scissor game"
			<<"\nHere are the rules :"
			<<"\n\tPaper covers rock"
			<<"\n\tRock breaks scissors"
			<<"\n\tScissors cut paper";
		// Ask two players enter 
		cout<<"\nPlayer one enter your letter (P,R or S):";
		cin>>one;
		cout<<"Player two enter your letter (P,R or S):";
		cin>>two;
		// Do if statement

		if ((one=='P'||one=='p') &&(two=='R'||two=='r')) 
		{cout<<"Player One is the winner because Paper covers rock"<<endl;

		won1++;
		total++;}

		else if ((one=='R'||one=='r') &&(two =='P'||two=='p'))

		{cout <<"Player Two is the winner because Paper covers rock";
		won2++;
		total++;}

		else if ((one=='S'||one=='s')&&(two=='R'||two=='r'))
		{cout<<"Player Two is the winner because Rock breaks scissors";
		won2++;
		total++;}
		else if ((one=='R'||one=='r') &&(two =='S'||two=='s'))
		{cout <<"Player One is the winner because Rock break scissors";
		won1;
		total++;}
		else if ((one=='P'||one=='p') &&(two=='S'||two=='s'))
		{cout <<"Player Two is the winner because Scissors cut paper";
		won2++;
		total++;}
		else if ((one=='S'||one=='s') &&(two=='P'||two=='p'))
		{cout <<"Player One is the winner because Scissors cut paper";
		won1++;
		total++;}
		else if

			((one=='s'||one=='S'||one=='p'||one=='P'||one=='r'||one=='R')&&
			(two=='s'||two=='S'||two=='p'||two=='P'||two=='r'||two=='R'))
		{cout<<"Nobody wins";
		noWon++;
		total++;
		}
		else
		{
			cout << "You have made an invalid enter ";
		}

		// Ask user play again
		cout<< "\nPlay again (y or n ) ?";
		cin>>answer;
	}while(answer=='y'||answer=='Y');
	// Summary Information

	if (answer=='n'||answer=='N')
		cout<< "\n\nWe played a total of " <<total<< " games "
		<< "\nPlayer One won " <<won1<< "game(s) "
		<< "\nPlayer Two won " <<won2<< "game(s) "
		<< "\nIn " <<noWon<< "game(s) nobody won ";
	else 
		cout<< "You have made an invalid enter"
		<<endl;
	//End the program
	return 0;
}

