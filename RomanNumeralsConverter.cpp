// Huy Ly
// Class TR at 9:00 am
// Homework Five
// Roman Numerals Converter
#include <iostream>
#include<string>
using namespace std;

/***************** PROTOTYPES FUNCTION*****************/
int Roman (int);
void PrintResults (int,string);
string thousands;
string hundreds;
string tens;
string ones;
string Numerals;
int year;

/******************************************************/

int main ()
{

	string RomanNumerals;

	char answer;
	do
	{
		// Ask user to enter a year
		cout << "\nEnter a number between 1 and 3,999: ";
		cin>>year;

		while(year<1||year>3999) // Loop until the user enter a true number
		{
			cout <<"Cannot convert that number: out of range \n";

		    cout <<"\nEnter a number between 1 and 3,999: ";
			cin>>year;
		}

		RomanNumerals=Roman(year);

		PrintResults(year,Numerals);	

cout<< "\nWould you like to enter another year? ";
		cin>>answer;

	}while(answer=='y');
	return 0;

}

int Roman(int)
{

// Do in thousands

	{	if (year/1000==3)  
			thousands="MMM";
		else if 
			(year/1000==2)
			thousands="MM";
		else if (year/1000==1)
			thousands="M";
		else 
			thousands=" ";}

// Do in hundreds

		{if ((year%1000)/100==9) 
			hundreds="CM";
		else if ((year%1000)/100==8)
			hundreds="DCCC";
		else if((year%1000)/100==7)
			hundreds="DCC";
		else if ((year%1000)/100==6)
			hundreds="DC";
		else if ((year%1000)/100==5)
			hundreds="D";
		else if ((year%1000)/100==4)
			hundreds="CD";
		else if ((year%1000)/100==3)
			hundreds="CCC";
		else if ((year%1000)/100==2)
			hundreds="CC";
		else if ((year%1000)/100==1)
			hundreds="C";
		else 
			hundreds="";}

// Do in tens

		{if (((year%1000)%100)/10==9) 
			tens="XC";
		else if (((year%1000)%100)/10==8)
			tens="LXXX";
		else if (((year%1000)%100)/10==7)
			tens="LXX";
		else if (((year%1000)%100)/10==6)
			tens="LX";
		else if (((year%1000)%100)/10==5)
			tens="L";
		else if (((year%1000)%100)/10==4)
			tens="XL";
		else if (((year%1000)%100)/10==3)
			tens="XXX";
		else if (((year%1000)%100)/10==2)
			tens="XX";
		else if (((year%1000)%100)/10==1)
			tens="X";
		else 
			tens="";}

// Do in ones

	{	if (((year%1000)%100)%10==9) 
			ones="IX";
		else if (((year%1000)%100)%10==8)
			ones="VIII";
		else if  (((year%1000)%100)%10==7)
			ones="VII";
		else if  (((year%1000)%100)%10==6)
			ones="VI";
		else if  (((year%1000)%100)%10==5)
			ones="V";
		else if  (((year%1000)%100)%10==4)
			ones="IV";
		else if  (((year%1000)%100)%10==3)
			ones="III";
		else if  (((year%1000)%100)%10==2)
			ones="II";
		else if  (((year%1000)%100)%10==1)
			ones="I";
		else
			ones="";}
	

	return year; // Come back where I called year from

}


void PrintResults(int Yr , string Num )

{
	string Numerals=thousands + hundreds+ tens+ ones; 

	cout << "The roman number equivalent of " <<year<<" is "<<Numerals;

	return ;
}
