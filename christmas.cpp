// Steven Ly
// Class TR at 9:00 am
// Lab Exercise 6
// Write a program that asks the user to enter a number between 1 and 12
// Then print a list of gifts that they will receive based on the Twelve Days of Christmas song

#include <iostream>
using namespace std;
int main ()
{
	// Set up memory
	int day;
	char answer;
	do{
		system("cls");
		// Ask user to enter day
		cout << "Please enter the day that you want to see the gifts for: ";
		cin>>day;

		switch (day)
		{
		case 12:
			cout<< "The gifts for that day of Christmas include: " 
				<<"\n\t12 Drummers Drumming"
				<< "\n\t11 Pipers Piping"
				<< "\n\t10 Lords A Leaping"
				<< "\n\t9 Ladies Dancing "
				<< "\n\t8 Maids a Milking "
				<< "\n\t7 Swans a Swimming "
				<< "\n\t6 Geese a Laying "
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 11:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t11 Pipers Piping"
				<< "\n\t10 Lords A Leaping"
				<< "\n\t9 Ladies Dancing "
				<< "\n\t8 Maids a Milking "
				<< "\n\t7 Swans a Swimming "
				<< "\n\t6 Geese a Laying "
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 10:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t10 Lords A Leaping"
				<< "\n\t9 Ladies Dancing "
				<< "\n\t8 Maids a Milking "
				<< "\n\t7 Swans a Swimming "
				<< "\n\t6 Geese a Laying "
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 9:
			cout<< "The gifts for that day of Christmas include: " 	
				<< "\n\t9 Ladies Dancing "
				<< "\n\t8 Maids a Milking "
				<< "\n\t7 Swans a Swimming "
				<< "\n\t6 Geese a Laying "
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 8:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t8 Maids a Milking "
				<< "\n\t7 Swans a Swimming "
				<< "\n\t6 Geese a Laying "
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 7:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t7 Swans a Swimming "
				<< "\n\t6 Geese a Laying "
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 6:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t6 Geese a Laying "
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 5:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t5 Golden Rings "
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 4:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t4 Calling Birds "
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 3:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t3 French Hens "
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 2:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\t2 Turtle Doves "
				<< "\n\tA partrige in a pear tree ";
			break;
		case 1:
			cout<< "The gifts for that day of Christmas include: " 
				<< "\n\tA partrige in a pear tree ";
		default:;
		}

		cout<< "\nDo you want to see another list? ";
		cin>>answer;
	}while(answer=='y');

	return 0;
}








