// Huy Ly
// Class at 9:00 am
// Lab Exercise Five
// Write a program that determines whether a meeting room is in violation of fire 
// law regulations regarding the maximum room capacity

#include<iostream>
using namespace std;
int main ()
{
	// Set up memory

	int const room=150;
	int people;
	int hold;
	int stayHome;
	// Print out to the screen

	cout<< "This program will determines if the room capacity is large enough to hold"
		<<"\nthe number of people planning to attend the meeting ";

	// Ask user to enter number of people

	cout<< "\n\nPlease enter the number of people planning on attending the meeting ";
	cin>>people;

	// Do calculation

	hold= room-people;
	stayHome=people-room;

	// Do if statement

	if (people<=room)
	{ 
		cout<< "According to the local fire code, the room can safely accomodate your needs";
		cout<< "\nIn fact, it can safely hold "<<hold<< " more people "
			<<endl;
	}
	else
	{
		cout<< "Iam sorry. According to fire code, the room capacity will be exceeded!!!"
			<< "\nPlease ask "<<stayHome<< " people to stay home "
			<<endl;
	}
	// End the program
	return 0;

}
